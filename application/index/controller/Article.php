<?php

namespace app\index\controller;

class Article extends Base
{
    /**
     * 每日一文
     */
    public function index()
    {
        // 接口调用
        $api_url = "https://interface.meiriyiwen.com/article/random";
        $api_con = file_get_contents($api_url);
        $api_con = json_decode($api_con,1);
        $this->assign('article',$api_con['data']);
        return $this->fetch();
    }
}
